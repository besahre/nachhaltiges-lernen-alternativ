import asyncio
import inspect
import json
from sanic import Sanic, Blueprint, response
from sanic.request import Request
from sanic.response import HTTPResponse
from typing import (
    Text,
    List,
    Dict,
    Any,
    Optional,
    Callable,
    Iterable,
    Awaitable,
    NoReturn,
)

from sqlalchemy import JSON

import rasa.utils.endpoints
from rasa.core.channels.channel import (
    InputChannel,
    CollectingOutputChannel,
    OutputChannel,
    UserMessage,
)

class MyIO(InputChannel):
    def name() -> Text:
        # Name of your custom channel.
        return "mattermostConnector"

    def name(name) -> Text:
        return "mattermostConnector"

    def blueprint(
        self, on_new_message: Callable[[UserMessage], Awaitable[None]]
    ) -> Blueprint:

        custom_webhook = Blueprint(
            "custom_webhook_{}".format(type(self).__name__),
            inspect.getmodule(self).__name__,
        )

        @custom_webhook.route("/", methods=["GET"])
        async def health(request: Request) -> HTTPResponse:
            return response.json({"status": "ok"})

        @custom_webhook.route("/webhook", methods=["POST"])
        async def receive(request: Request) -> HTTPResponse:
            if str(request.content_type) == "application/json":
                print(request.json)
                request_json = request.json
                sender_id = request_json["user_name"] # method to get sender_id
                request_type = "by_button"
                context = request_json["context"]
                text = context["text"] # method to fetch text
            else:
                request_form = request.form
                sender_id = request_form.get("user_name") # method to get sender_id 
                text = request_form.get("text") # method to fetch text
                request_type = "by_slashcommand"
            input_channel = self.name() # method to fetch input channel
            metadata = self.get_metadata(request) # method to get metadata

            output = Mattermost_Output()
            
            # include exception handling

            
            output.store_user_input(sender_id, text, request_type)
            await on_new_message(
                UserMessage(
                    text,
                    output,
                    sender_id,
                    input_channel=input_channel,
                    metadata=metadata,
                )
            )
            return response.json(output.response_message())

        return custom_webhook

class Mattermost_Output(OutputChannel):
    """Output channel that sends both the original message and the response as json"""
    
    def __init__(self) -> None:
        self.messages = []
        self.response = ""
        self.response2 = ""
        self.user_name = ""
        self.user_text = ""
        self.extra_responses = []
        self.request_type = ""
        self.myname = "Nachhaltiges Lernen"
        self.response_type = "in_channel"
        self.myicon =  "https://i.ibb.co/XXrVwYV/robot-icons-30475.png"


    @classmethod
    def name(cls) -> Text:
        return "mattermost_output"

    def store_user_input(self, username: Text, user_text: Text, request_type: Text) -> None:
        print(str(request_type))
        print("My dear friend " + username + "said to me: '" + user_text + "'")
        self.user_name = username
        self.user_text = user_text
        self.request_type = request_type

    def response_message(self):
        value = {
            "username" : self.user_name,
            "text" : self.user_text,
            "response_type" : self.response_type,
            "extra_responses": self.extra_responses
        }
        return value

    async def send_text_message(
        self, recipient_id: Text, text: Text, **kwargs: Any
    ) -> None:
        print("I responded to my dear friend " + str(recipient_id) + " the following words: \n '" + str(text) + "'")
        # Zum Senden von JSON Konstrukten (muss die Keys "text" und "attachements" enthalten)  an Mattermost
        if text.find("{")!= -1:
            my_json = json.loads(text)
            attachment = my_json["attachments"]
            text = my_json["text"]
        else:
        # Zum Senden von plaintext an Mattermost
            attachment =[]
        response_to_append = {
            "username" : self.myname,
            "text" : text,
            "response_type" : self.response_type,
            "icon_url": self.myicon,
            "attachments": attachment
        }

        self.extra_responses.append(response_to_append)   


