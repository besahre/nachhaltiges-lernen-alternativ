# nachhaltiges-lernen-chatbot
Mit dieser Anleitung kann der Rasa Chatbot mithilfe von Slash-Befehlen angesprochen werden und mithilfe von Buttons und eingehenden Webhooks Nachrichten an Mattermost zurücksenden. Voraussetzung ist eine laufende Mattermost - Instanz (e.g. https://docs.mattermost.com/install/install-docker.html).

## Getting started
Folgende Schritte müssen durchgeführt werden:
 - [ ] Einstellen der Ports
 - [ ] Einstellen der IP
 - [ ] Erstellen eines Slash-Befehls in Mattermost
 - [ ] Erstellen eines Nutzers namens "nachhaltiges_lernen" mit Admin-Rechten
 - [ ] Erstellen eines eingehenden Webhooks mit dem Nutzer "nachhaltiges_lernen" in Mattermost
 - [ ] Anpassen der Datei editable_config.json
 - [ ] Starten von RASA Core und RASA Action Server

##  Einstellen der Ports
Im Allgemeinen können die default-Ports verwendet werden, außer es laufen bereits andere Anwendungen darunter.
RASA: 
 - [ ] Standardmäßig ist hier für den Core-Server der Port 5007 eingestellt und ist an 2 Stellen anpassbar
 - [ ] In der Datei editable_config.json muss unter dem key rasa_webhook_url der Port in "http://<IP>:<PORT>/webhooks/mattermostConnector/webhook" eingefügt werden.
 - [ ] Beim Starten von RASA muss der Port mit -p <PORT> angehängt werden:
```
rasa run --enable-api --cors '*' -p 5007
```
## Einstellen der IP
Es muss in der Datei editable_config.json die IP angegeben werden, auf der das entsprechende System (einmal RASA-Core, einmal Mattermost) läuft und erreichbar ist. 
localhost reicht da nicht aus, es muss die richtige IP Adresse sein. Die IP von RASA-Core muss in Mattermost unter Systemkonfiguration - Entwickler - "AllowUntrustedConnections" freigeschaltet werden.

## Erstellen eines Slash-Befehls in Mattermost
Siehe Tutorial unter tutorials/Tutorial für die Erstellung eines SlashBefehls.doc

## Erstellen eines eingehenden Webhooks in Mattermost
Dazu muss der Nutzer "nachhaltiges_lernen" eingeloggt sein. Über den Nutzer, der den Webhook erstellt, laufen die Konversationen der Studie.
Siehe Tutorial unter tutorials/Tutorial für die Erstellung eines eingehenden Webhooks.doc

Das im Bestätigungsbildschirm erhaltene Token kopieren für das
## Anpassen der Datei editable_config.json
Token an die Stelle des Tokens von mattermost_webhook_url kopieren:
```
"mattermost_webhook_url" : "http://192.168.0.145:8065/hooks/<TOKEN>"
```

## Starten von RASA Core und RASA Action Server
Benötigt werden RASA Version 2.8 und Python 3.7

Beim Starten der beiden RASA Systeme muss der Port mit -p übergeben werden:
```
rasa run --enable-api --cors '*' -p 5007
```

Hinweis: Sollten der Action Server und RASA Core nicht auf dem gleichen System laufen, muss die Datei endpoints.yml auf die entsprechende IP und Port angepasst werden.
```
rasa run actions -p 5057
```