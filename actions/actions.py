# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions



from ast import Num
from numbers import Number
from tracemalloc import start
from typing import Any, Text, Dict, List
from datetime import date, datetime, timedelta
from xml import dom
import requests
from sqlalchemy import DateTime, Float, false
import json

from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet, AllSlotsReset, FollowupAction, ReminderScheduled
from rasa_sdk.executor import CollectingDispatcher

# Als Action registriert damit es am Anfang initialisiert wird
class SetUpConfigFile(Action):
    def name(self) -> Text:
        with open('editable_config.json') as data_file:
            data_loaded = json.load(data_file)
        global mattermost_webhook_url
        mattermost_webhook_url = data_loaded["mattermost_webhook_url"]
        global rasa_webhook_url
        rasa_webhook_url = data_loaded["rasa_webhook_url"]
        global lime_survey_link
        lime_survey_link = data_loaded["lime_survey_link"]
        global number_of_group1
        number_of_group1 = 0
        global number_of_group2
        number_of_group2 = 0
        global number_of_participant
        number_of_participant = 2000 
        global map_of_participants
        map_of_participants = {}
        print("Mattermost_Webhook: " + str(mattermost_webhook_url))
        print("Rasa Webhook:  " + str(rasa_webhook_url))
        return "read_config"

class ProduceWelcomeMessage(Action):
    def name(self) -> Text:
        return "action_produce_welcome_message"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        attachement2 = [
                    {
                    "text": "",
                    "image_url": "https://i.ibb.co/S59xGk6/studienstart-chad-message.png"
                    },
                    {
                    "actions": [
                        {
                            "name": "Start",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/greet",
                                }
                            }
                        }
                    ]
                    }
                ] 
        message = "Herzlich Willkommen zur Studie 'Nachhaltiges Lernen mit Chatbots'! \n Zum Starten der Studie klicken Sie bitte auf Start.\n"
        message += "Hinweis zum Datenschutz: \n Es werden keine IP-Adressen erfasst."
        message += "Alle gesammelten Daten werden lediglich zur Forschung verwendet.\n"
        message += "Mit dem Klick auf Start bestätigen Sie, dass Sie die Datenschutzerklärung gelesen und akzeptiert haben. "
        message += "Bei Fragen zum Datenschutz oder zur Studie stehe ich selbstverständlich unter 01573 7030788 (e.g. Whatsapp) zur Verfügung und freue mich persönlich sehr über jede Person, die an der Studie teilnimmt! "
        message += "\n\nSobald Sie auf Start geklickt haben, wird Chad Sie kontaktieren. Sie können auf der linken Seite den Chat von Chad auswählen."
        attachement = [
                    {
                    "actions": [
                        {
                            "name": "Start",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "/greet",
                                }
                            }
                        }
                    ]
                    }
                ] 
        payload = {
            "channel": "off-topic", 
            "text": message,
            "icon_url": "https://i.ibb.co/XXrVwYV/robot-icons-30475.png",
            "username" : "Nachhaltiges Lernen",
            "attachments": attachement
            }
        payload2 = {
            "channel": "town-square", 
            "text": message,
            "icon_url": "https://i.ibb.co/XXrVwYV/robot-icons-30475.png",
            "username" : "Nachhaltiges Lernen",
            "attachments": attachement2
            }
        requests.post(mattermost_webhook_url, json = payload2)
        requests.post(mattermost_webhook_url, json = payload)
        return
        
class SetUpAction(Action):
    def name(self) -> Text:
        return "action_setup"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        sender_id = tracker.current_state()['sender_id']

        global number_of_participant
        global map_of_participants
        events = []
        if not tracker.slots.get("username") == sender_id:
            map_of_participants[sender_id] = number_of_participant
            number_of_participant += 1

            print("Set slot username for " + sender_id)
            events.append(SlotSet('username', sender_id))
            global number_of_group1
            global number_of_group2
            if number_of_group1 < number_of_group2:
                group = "1"
                number_of_group1 = number_of_group1 + 1
            else: 
                group = "2"
                number_of_group2 = number_of_group2 + 1
            events.append(SlotSet('group', group))
            self.sendGreetings(sender_id)
        return events
    def sendGreetings(self, username: Text) -> None:
        attachement = [
                    {
                    "actions": [
                        {
                            "name": "Los gehts!",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "start",
                                }
                            }
                        }
                    ]
                    }
                ]
        text = "Hi. Ich bin Chad und freue mich sehr, dass Sie sich für diese Studie Zeit genommen haben. \n\n"
        text +="Ich helfe Ihnen, in dieser Studie über zwei fiktive Situationen nachzudenken. Ihre Aufgabe ist es, sich in diese Situationen hineinzuversetzen, als hätten Sie diese wirklich erlebt.\n"
        text += "Anschließend stelle ich Ihnen zu Ihrer 'erlebten' Situation einige Fragen. \n"
        text += "Formulieren Sie alle Antworten aus der Perspektive einer Person, die diese Situation wirklich erlebt hat."
        OutgoingWebhook.send_via_webhook(
            message=text,
            receiver=username, 
            attachment=attachement)


class OutgoingWebhook(Action):
    def name(self) -> Text:
        return "outgoing_webhook"

    @staticmethod
    def send_via_webhook(message: Text, receiver: Text, attachment: List[Dict[Text, Any]]):
        payload = {
            "channel": "@" + str(receiver), 
            "text": message,
            "icon_url": "https://i.ibb.co/XXrVwYV/robot-icons-30475.png",
            "username" : "Chad",
            "attachments": attachment
            }
        requests.post(mattermost_webhook_url, json = payload)
        return
    
    @staticmethod
    def getAttachementFinish() -> List[Dict[Text, Any]]:
        return [
                    {
                    "actions": [
                        {
                            "name": "Ich bin fertig!",
                            "integration": {
                                "url": rasa_webhook_url,
                                "context": {
                                    "text": "weiter",
                                }
                            }
                        }
                    ]
                    }
                ]
    

        
class SubmitReflectionSessionAction(Action):

    def name(self) -> Text:
        return "action_submit_reflection_session"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        username = tracker.current_state()['sender_id']
        events = []
        events.append(SlotSet("judgement", None))
        events.append(SlotSet("emotions", None))
        events.append(SlotSet("learning", None))
        events.append(SlotSet("planning", None))

        number_of_session = tracker.get_slot("number_of_session") + 1.0
        events.append(SlotSet('number_of_session', number_of_session ))

        print("Number of Session: " + str(number_of_session))
        if number_of_session >= 2.0:
            global map_of_participants
            group = tracker.slots.get("group")
            suffix = "&G1Q1=" + str(map_of_participants[username]) + "&G1Q2=" + group + "&newtest=Y"
            global lime_survey_link
            text= "Vielen Dank für die Teilnahme an dieser Studie! Wir bitten Sie zum Abschluss der Studie diese kurze Umfrage auszufüllen: " + lime_survey_link + suffix
            attachement = [
                {
                "actions": [
                    {
                        "name": "Eine weitere Reflexionssitzung durchführen (Bonus)",
                        "integration": {
                            "url": rasa_webhook_url,
                            "context": {
                                "text": "start",
                            }
                        }
                    }
                ]
                }
            ]
            OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=attachement)
        else:
            text= "Das war die erste der beiden Situationen. Es war mir eine Freude. Bereit für die zweite Situation?"
            attachement = [
                {
                "actions": [
                    {
                        "name": "Weiter gehts!",
                        "integration": {
                            "url": rasa_webhook_url,
                            "context": {
                                "text": "start",
                            }
                        }
                    }
                ]
                }
            ]
            OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=attachement )

        return events

class AskForJudgementAction(Action):
    def name(self) -> Text:
        return "action_ask_reflection_form_judgement"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[Dict[Text, Any]]:
        username = tracker.current_state()['sender_id']
        events = []
        # Wähle Name je nach Session
        name= ReflectionThema.getNameForSession(tracker)
            
        if tracker.slots.get("group")=="1":
            text = "Guten Tag Dr. " + name + ",\nbitte nehmen Sie sich sieben Minuten Zeit, um über Ihre Situation nachzudenken. Schreiben Sie alle Ihre Gedanken, Ideen und Überlegungen in den Chat. \n"
            events.append(SlotSet("judgement", ""))
            events.append(SlotSet("emotions", ""))
            events.append(SlotSet("learning", ""))
            events.append(SlotSet("planning", ""))
        else:
            text = "Guten Tag Dr. " + name + ", \n ich würde Ihnen gerne helfen, über die Situation nachzudenken, und insgesamt vier Fragen stellen. \n"
            text += "\n**Wie bewerten Sie Ihre Situation (siehe oben) und Ihr Handeln in der Situation?**\n"

        text += "\nWenn Sie alle Ihre Gedanken in den Chat geschrieben haben, drücken Sie Enter und anschließend auf den Button \"Ich bin fertig!\"."
        OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=OutgoingWebhook.getAttachementFinish())
        return events

class AskForEmotionAction(Action):
    def name(self) -> Text:
        return "action_ask_reflection_form_emotions"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[Dict[Text, Any]]:
        username = tracker.current_state()['sender_id']

        name = ReflectionThema.getNameForSession(tracker)
        text = "**" + name + ", welche Emotionen spüren Sie, wenn Sie sich in Ihre erlebte Situation zurückversetzen?**\n"
        OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=OutgoingWebhook.getAttachementFinish())

        return []

class AskForLearningAction(Action):
    def name(self) -> Text:
        return "action_ask_reflection_form_learning"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[Dict[Text, Any]]:
        username = tracker.current_state()['sender_id']

        name = ReflectionThema.getNameForSession(tracker)
        text = "**Welche neue Erkenntnisse haben Sie, " + name + ", durch das Nachdenken über Ihre Situation erlangt?**\n"
        OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=OutgoingWebhook.getAttachementFinish())
        return []

class AskForPlanningAction(Action):
    def name(self) -> Text:
        return "action_ask_reflection_form_planning"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[Dict[Text, Any]]:
        username = tracker.current_state()['sender_id']

        name= ReflectionThema.getNameForSession(tracker)
        text = "**" + name + ", wie würden Sie jetzt anders handeln, wenn Sie in eine ähnliche Situation kommen sollten?**\n"
        OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=OutgoingWebhook.getAttachementFinish())
        return []

class ReflectionThema(Action):
    def name(self) -> Text:
        return "action_reflection_thema"
    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[Dict[Text, Any]]:
        username = tracker.current_state()['sender_id']
        number_of_session = tracker.get_slot("number_of_session")
        text = self.getReflectionTheme(number_of_session)
        attachments = self.getAttachements(number_of_session)
        OutgoingWebhook.send_via_webhook(message=text, receiver=username, attachment=attachments)
        return

    def getAttachements(self, number_of_session: Float) -> List[Dict[Text, Any]]:
        if number_of_session==5.0:
            return [
                    {
                    "text": "Die von Eberhardt erhaltene Mail:",
                    "image_url": "https://i.ibb.co/bbZKgSN/2022-07-07-11-23-31-Widerherstellung-Passworts-erforderlich.png"
                    }
                ] 
        else:
            return []
    @staticmethod
    def getNameForSession(tracker: Tracker) -> Text:
        number_of_session = tracker.slots.get("number_of_session")
        if number_of_session==0.0 or number_of_session==2.0 or number_of_session==5.0:
            return "Eberhardt"
        else:
            return "Vanessa"
    def getReflectionTheme(self, number_of_session: Float) -> Text:
        if number_of_session==0.0:
            return "Sie sind Doktor Eberhardt, Assistenzarzt der Charité.\n\n Um den Assistenzärzt:innen Zugriff auf das Computersystem von außen zu ermöglichen, wird durch die IT der Klinik ein VPN eingerichtet. \nAlle Assistenzärzt:innen können dadurch - mit Name und Passwort - von zu Hause aus auf das System und auf die enthaltenen vertraulichen Daten ihrer Patienten zugreifen. \nAssistenzärztin Vanessa wählt ein kompliziertes Passwort. \nAm nächsten Tag klagt sie darüber, dass sie ihr Passwort vergessen hat. \n\nDer Assistenzarzt Eberhardt kann darüber nur schmunzeln. Er wählte für seinen Account das Passwort \"12345\".\n\n\nZwei Wochen später folgt eine Rundmail der IT, dass es einen Hacker-Angriff gegeben hat, bei dem Daten gestohlen wurden. Eberhardt denkt an die stattgefundene Situation zurück."
        if number_of_session==1.0:
            return "Sie sind nun Doktor Vanessa, Assistenzärztin der Charité. \n\nDie Assistenzärztin Vanessa meldet sich über das VPN in ihrer Klinik an, um sich ihren Terminkalender anzuschauen – der Chefarzt sprach mit ihr am Nachmittag über einen Termin, der angepasst wurde. \n\nBei der Anmeldung im VPN fällt ihr auf, dass sie sich auch ohne Passwort, nur mit Angabe ihres Benutzernamens, anmelden kann. \nVanessa freut sich, weil sie sowieso ihr Passwort vergessen hatte. \n\nSie weiß, dass sie morgen noch mehrfach über das VPN auf das System zugreifen muss, daher beschließt sie, die fehlende Passwortabfrage erst in zwei Tagen der IT zu melden."
        if number_of_session==2.0:
            return "Die elektronische Patientenakte (ePA) gibt es seit Anfang 2021. Diese soll die bessere Verfügbarkeit von Patientendaten gewährleisten. \n Der Assistenzarzt Eberhardt hat soeben das Ergebnis eines therapeutischen Gesprächs mit einer Patientin über die Ursachen ihrer Bulimia nervosa in die ePA hochgeladen.\n Dann liest er folgenden Zeitungsartikel, der ihm von einem Kollegen empfohlen wurde: \n\n\"In Finnland wurden Ende 2018 Patientendaten aus 20 Psychotherapiekliniken gestohlen. Nach erfolgloser Erpressung der Kliniken wurden tausende Patienten mit der Drohung erpresst, dass der Inhalt der therapeutischen Gespräche veröffentlicht werde, sollten die Betroffenen kein Lösegeld zahlen.\" \n\nDer Artikel lässt Eberhardt überlegen, welche Daten alle in die ePA hochgeladen werden sollten. "
        if number_of_session==3.0:
            return "Assistenzärztin Vanessa nimmt bei der Anamnese Daten ihres Patienten Alfred auf. Sie hat die Wahl, zu Forschungszwecken deutlich mehr Daten aufzunehmen, als eigentlich zur Behandlung des Patienten notwendig.\n Zwar ist im Kleingedruckten des zusätzlichen Anamnesebogens angegeben, dass die Angaben für den Patienten optional sind und nicht explizit für die Behandlung benötigt werden, aber Vanessa hat ihre Zweifel, ob der Patient angemessen darüber unterrichtet wurde.\n Sie beschließt, dass das nicht ihre Aufgabe ist und fährt mit der Behandlung fort."
        if number_of_session==4.0:
            return "Assistenzärztin Vanessa schreibt Assistenzarzt Eberhardt über WhatsApp wegen der Aufnahme von Forschungsdaten bei der Anamnese ihres Patienten. Eberhardt bestätigt ihre Bedenken und empfiehlt ein aufklärendes Gespräch bezüglich der Forschungsdaten, auch wenn das mehr Zeit beansprucht. \nAnschließend fragt Eberhardt Vanessa, ob er die psychologischen Gutachten seines Patienten in die elektronische Patientenakte hochladen sollte. Er beschreibt dabei genau, welche Inhalte im psychologischen Gutachten vorhanden sind, die er als kritisch sieht."
        if number_of_session==5.0:
            return "Der Assistenzarzt Eberhardt bekommt eine E-Mail mit \"wiederherstellung vom ihrem password erforderlich\" im Betreff (siehe Bild unten).  \n\n Eberhardt klickt auf den in der Mail bereitgestellten Link und wird auf eine Website weitergeleitet, die genauso aussieht wie das Mattermost-System seiner Klinik. Allerdings enthält die URL oben im Browser weder das Wort Mattermost, noch ist das \"Schloss-Symbol\" sichtbar. Eberhardt gibt in die erforderlichen Felder sein Passwort ein und klickt auf \"Passwort wiederherstellen\". \nZwei Wochen später folgt eine Rundmail der IT, dass es einen Hacker-Angriff gegeben hat, bei dem Daten gestohlen wurden. Eberhardt denkt an die stattgefundene Situation zurück."
        return "Sie haben bereits alle Szenarien erlebt. Vielen Dank für die Teilnahme an dieser Studie! Ich bitte Sie zum Abschluss der Studie die Umfrage auszufüllen."